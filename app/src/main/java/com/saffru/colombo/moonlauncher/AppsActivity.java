package com.saffru.colombo.moonlauncher;

import androidx.appcompat.app.AppCompatActivity;

import android.content.ComponentName;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class AppsActivity extends AppCompatActivity {

    List<String> list_className = new ArrayList<>();

    private AutoCompleteTextView autoTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_drawer_all);

        Intent intent = new Intent()
                .setAction(Intent.ACTION_MAIN)
                .addCategory(Intent.CATEGORY_LAUNCHER);
        PackageManager pm = getPackageManager();
        List<ResolveInfo> list = pm.queryIntentActivities(intent,
                PackageManager.GET_META_DATA);

        autoTextView = findViewById(R.id.autoTextView);

        List<ApplicationInfo> appInfoList = new ArrayList<>();
        for (ResolveInfo info : list) {
            ApplicationInfo appInfo = null;
            try {
                appInfo = pm.getApplicationInfo(info.activityInfo.packageName, PackageManager.GET_META_DATA);
            } catch (PackageManager.NameNotFoundException e) {
                e.printStackTrace();
            }
            appInfoList.add(appInfo);
        }
        Collections.sort(appInfoList, new ApplicationInfo.DisplayNameComparator(pm));

        final ListView listView = findViewById(R.id.listview);
        List<String> myStrinrgArray = new ArrayList<>();
        List<Drawable> listIcons = new ArrayList<>();

        for (ApplicationInfo applicationInfo : appInfoList) {
            String appName = pm.getApplicationLabel(applicationInfo).toString();
            Drawable icon = pm.getApplicationIcon(applicationInfo);
            String className = applicationInfo.packageName;
            myStrinrgArray.add(appName);
            listIcons.add(icon);
            list_className.add(className);
        }

        final MyAdapterAll adapter = new MyAdapterAll(AppsActivity.this,
                myStrinrgArray,
                listIcons, list_className);

        listView.setAdapter(adapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent intent;
                PackageManager pm = getPackageManager();

                intent = pm.getLaunchIntentForPackage(list_className.get(i));
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                finish();

            }
        });

        TextView back = findViewById(R.id.textView9);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        TextView reset = findViewById(R.id.textView10);
        reset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PreferenceManager.getDefaultSharedPreferences(AppsActivity.this).edit().clear().apply();
                startActivity(new Intent(AppsActivity.this,
                        MainActivity.class));
            }
        });

        TextView tv_quit = findViewById(R.id.textView11);
        tv_quit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(isMyLauncherDefault()) {
                    startActivity(new Intent(AppsActivity.this,
                            MainActivity.class));

                    PackageManager p = getPackageManager();
                    ComponentName cN = new ComponentName(AppsActivity.this,
                            FakeLauncherActivity.class);
                    p.setComponentEnabledSetting(cN, PackageManager.COMPONENT_ENABLED_STATE_ENABLED, PackageManager.DONT_KILL_APP);

                    Intent selector = new Intent(Intent.ACTION_MAIN);
                    selector.addCategory(Intent.CATEGORY_HOME);
                    selector.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);

                    startActivity(selector);

                    p.setComponentEnabledSetting(cN, PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP);

                }
            }
        });

        autoTextView.setThreshold(1); //will start working from first character

        final AdapterSearch adapter_search =
                new AdapterSearch(AppsActivity.this,
                myStrinrgArray, list_className);

        autoTextView.setAdapter(adapter_search);
    }

    boolean isMyLauncherDefault() {
        PackageManager localPackageManager = getPackageManager();
        Intent intent = new Intent("android.intent.action.MAIN");
        intent.addCategory("android.intent.category.HOME");
        String str = localPackageManager.resolveActivity(intent,
                PackageManager.MATCH_DEFAULT_ONLY).activityInfo.packageName;
        return str.equals(getPackageName());
    }
}
