package com.saffru.colombo.moonlauncher;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.ActionMode;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AbsListView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private int nr = 0;

    TextView num;
    Button btn_done;
    List<String> list_className = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        num = findViewById(R.id.num);

        btn_done = findViewById(R.id.button);

        PreferenceManager.getDefaultSharedPreferences(MainActivity.this).edit().clear().apply();

        Intent intent = new Intent()
                .setAction(Intent.ACTION_MAIN)
                .addCategory(Intent.CATEGORY_LAUNCHER);
        PackageManager pm = getPackageManager();
        List<ResolveInfo> list = pm.queryIntentActivities(intent,
                PackageManager.GET_META_DATA);
        List<ApplicationInfo> appInfoList = new ArrayList<>();
        for (ResolveInfo info : list) {
            ApplicationInfo appInfo = null;
            try {
                appInfo = pm.getApplicationInfo(info.activityInfo.packageName, PackageManager.GET_META_DATA);
            } catch (PackageManager.NameNotFoundException e) {
                e.printStackTrace();
            }
            appInfoList.add(appInfo);
        }
        Collections.sort(appInfoList, new ApplicationInfo.DisplayNameComparator(pm));

        final ListView listView = findViewById(R.id.listview);
        List<String> myStrinrgArray = new ArrayList<>();
        List<Drawable> listIcons = new ArrayList<>();

        for (ApplicationInfo applicationInfo : appInfoList) {
            String appName = pm.getApplicationLabel(applicationInfo).toString();
            Drawable icon = pm.getApplicationIcon(applicationInfo);
            String className = applicationInfo.packageName;
            myStrinrgArray.add(appName);
            listIcons.add(icon);
            list_className.add(className);
        }

        final MyAdapter adapter = new MyAdapter(MainActivity.this, myStrinrgArray,
                listIcons, list_className);
        adapter.clearSelection();

        listView.setAdapter(adapter);
        listView.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE_MODAL);
        listView.setMultiChoiceModeListener(new AbsListView.MultiChoiceModeListener() {

            @Override
            public boolean onCreateActionMode(ActionMode actionMode, Menu menu) {
                menu.clear();
                return true;
            }

            @Override
            public boolean onPrepareActionMode(ActionMode actionMode, Menu menu) {
                return true;
            }

            @Override
            public boolean onActionItemClicked(ActionMode actionMode, MenuItem menuItem) {
                menuItem.setVisible(false);
                return true;
            }

            @Override
            public void onDestroyActionMode(ActionMode actionMode) {
                nr = 0;
                num.setText(String.valueOf(nr));
                adapter.clearSelection();
            }

            @Override
            public void onItemCheckedStateChanged(ActionMode mode,
                                                  int position, long id, boolean checked) {
                SharedPreferences prefs =
                        PreferenceManager.getDefaultSharedPreferences(MainActivity.this);

                SharedPreferences.Editor editor = prefs.edit();

                if (checked) {
                    if(nr<8){
                        nr++;
                        adapter.setNewSelection(position, checked);
                        num.setText(String.valueOf(nr));

                        /*editor.putString(String.valueOf(nr), list_className.get(position));
                        editor.apply();*/
                    }
                } else {
                    /*editor.putString(String.valueOf(nr), "");
                    editor.apply();*/
                    nr--;
                    num.setText(String.valueOf(nr));
                    adapter.removeSelection(position);
                }

            }
        });

        btn_done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(nr<=8){
                    MyAdapter.setPref();
                    startActivity(new Intent(MainActivity.this, Drawer8Activity.class));
                }
            }
        });
    }


}
