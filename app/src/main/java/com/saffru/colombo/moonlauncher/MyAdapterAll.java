package com.saffru.colombo.moonlauncher;


import android.content.Context;
import android.graphics.Color;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.core.content.res.ResourcesCompat;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class MyAdapterAll extends BaseAdapter {
    private List<String> list_nomi, list_className;
    List<Drawable> listIcons;
    Context context;

    private HashMap<Integer, Boolean> mSelection = new HashMap<>();

    MyAdapterAll(Context context,
                 List<String> list_nomi, List<Drawable> listIcons, List<String> list_className){
        this.context = context;
        this.list_nomi = list_nomi;
        this.listIcons = listIcons;
        this.list_className = list_className;

    }

    @Override
    public int getCount() {
        return list_nomi.size();
    }

    @Override
    public Object getItem(int i) {
        return list_nomi.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        view = LayoutInflater.from(context).
                inflate(R.layout.item_all_app, viewGroup, false);
        view.setBackgroundColor(Color.parseColor("#000000")); //default color
        if (mSelection.get(i) != null) {
            view.setBackgroundColor(Color.RED);// this is a selected position so make it red
        }

        if (mSelection.get(i) != null) {
            view.setBackgroundColor(Color.RED);// this is a selected position
            // so make it red
        }
        TextView tv_nome = view.findViewById(R.id.nome);
        ImageView icon = view.findViewById(R.id.imageView2);
        Typeface typeface = ResourcesCompat.getFont(context, R.font.mina_regular);
        tv_nome.setTypeface(typeface);
        ColorMatrix matrix = new ColorMatrix();
        matrix.setSaturation(0);

        ColorMatrixColorFilter filter = new ColorMatrixColorFilter(matrix);
        icon.setColorFilter(filter);

        icon.setImageDrawable(listIcons.get(i));
        tv_nome.setText(list_nomi.get(i));


        return view;
    }

    void setNewSelection(int position, boolean checked) {
        mSelection.put(position, checked);
        notifyDataSetChanged();
    }

    void removeSelection(int position) {
        mSelection.remove(position);
        notifyDataSetChanged();
    }

    void clearSelection() {
        mSelection = new HashMap<>();
        notifyDataSetChanged();
    }

    boolean isPositionChecked(int position) {
        Boolean result = mSelection.get(position);
        return result == null ? false : result;
    }

    private static class ViewHolder {
        TextView autoText;
    }
}
