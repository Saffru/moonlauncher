package com.saffru.colombo.moonlauncher;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.core.content.res.ResourcesCompat;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class AdapterSearch extends BaseAdapter implements Filterable {
    private List<String> list_nomi, list_className;
    static Context context;
    private ArrayList<Object> suggestions = new ArrayList<>();
    private Filter filter = new AdapterSearch.CustomFilter();
    Intent intent;
    PackageManager pm;

    AdapterSearch(Context context,
                 List<String> list_nomi, List<String> list_className){
        this.context = context;
        this.list_nomi = list_nomi;
        this.list_className = list_className;
        pm = context.getPackageManager();
    }

    @Override
    public int getCount() {
        return suggestions.size(); // Return the size of the suggestions list.
    }
    @Override
    public Object getItem(int i) {
        return suggestions.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(final int i, View view, ViewGroup viewGroup) {
        view = LayoutInflater.from(context).
                inflate(R.layout.item_search, viewGroup, false);

        TextView tv_nome = view.findViewById(R.id.nome);
        tv_nome.setText(suggestions.get(i).toString());
        Typeface typeface = ResourcesCompat.getFont(context, R.font.mina_regular);
        tv_nome.setTypeface(typeface);

        tv_nome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                intent =
                        pm.getLaunchIntentForPackage(getPackNameByAppName(context, suggestions.get(i).toString()));
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                context.startActivity(intent);
                ((Activity)context).finish();
            }
        });

        return view;
    }

    public static String getPackNameByAppName(Context context, String name) {
        PackageManager pm = context.getPackageManager();
        List<ApplicationInfo> l = pm.getInstalledApplications(PackageManager.GET_META_DATA);
        String packName = "";
        for (ApplicationInfo ai : l) {
            String n = (String)pm.getApplicationLabel(ai);
            if (n.contains(name) || name.contains(n)){
                packName = ai.packageName;
            }
        }

        return packName;
    }

    @Override
    public Filter getFilter() {
        return filter;
    }

    private class CustomFilter extends Filter {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            suggestions.clear();
            String s = constraint.toString().toLowerCase();
            if (list_nomi != null && constraint != null) { // Check if the Original
                // List and Constraint aren't null.
                for (int i = 0; i < list_nomi.size(); i++) {
                    if (list_nomi.get(i).toLowerCase().contains(s)) { // Compare
                        // item in original list if it contains constraints.
                        suggestions.add(list_nomi.get(i)); // If TRUE add item in Suggestions.
                    }
                }
            }
            FilterResults results = new FilterResults(); // Create new Filter Results and return this to publishResults;
            results.values = suggestions;
            results.count = suggestions.size();

            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            if (results.count > 0) {
                notifyDataSetChanged();
            } else {
                notifyDataSetInvalidated();
            }
        }
    }
}
