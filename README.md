Moon Launcher is a digital wellbeing experiments for Android devices.
It help you to stay focused on your essentials applications.

Watch the [YouTube Video](Moon Launcher helps you find focus, you can choose your 8 favorite applications and set up them in your launcher.)

You can choos a maximum of 8 applications and add them to your launcher.

This app is also thinked for work with AMOLED display. The app bring you in a dark mode, in this mode you battery can during a lot.

[Download](https://play.google.com/store/apps/details?id=com.saffru.colombo.moonlauncher) the app and try the experiment.
