package com.saffru.colombo.moonlauncher;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class Drawer8Activity extends AppCompatActivity {

    TextView tv1, tv2, tv3, tv4, tv5, tv6, tv7, tv8, tv_date;
    SharedPreferences prefs;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_drawer);

        prefs = PreferenceManager.getDefaultSharedPreferences(this);

        tv1 = findViewById(R.id.textView1);
        tv2 = findViewById(R.id.textView2);
        tv3 = findViewById(R.id.textView3);
        tv4 = findViewById(R.id.textView4);
        tv5 = findViewById(R.id.textView5);
        tv6 = findViewById(R.id.textView6);
        tv7 = findViewById(R.id.textView7);
        tv8 = findViewById(R.id.textView8);

        tv_date = findViewById(R.id.textView15);
        Date today = Calendar.getInstance().getTime();//getting date
        SimpleDateFormat formatter = new SimpleDateFormat("E dd MMM");//formating according to my need
        String date = formatter.format(today);
        tv_date.setText(date);

        getandset(tv1, "1");
        getandset(tv2, "2");
        getandset(tv3, "3");
        getandset(tv4, "4");
        getandset(tv5, "5");
        getandset(tv6, "6");
        getandset(tv7, "7");
        getandset(tv8, "8");

        tv1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent;
                PackageManager pm = getPackageManager();

                String pckName = prefs.getString("1",
                        null);
                if(pckName!=null){
                    intent =
                            pm.getLaunchIntentForPackage(AdapterSearch.getPackNameByAppName(Drawer8Activity.this, pckName));
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                }

            }
        });
        tv2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent;
                PackageManager pm = getPackageManager();

                String pckName = prefs.getString("2",
                        null);
                if(pckName!=null){
                    intent =
                            pm.getLaunchIntentForPackage(AdapterSearch.getPackNameByAppName(Drawer8Activity.this,pckName));
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                }

            }
        });
        tv3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent;
                PackageManager pm = getPackageManager();

                String pckName = prefs.getString("3",
                        null);
                if(pckName!=null){
                    intent =
                            pm.getLaunchIntentForPackage(AdapterSearch.getPackNameByAppName(Drawer8Activity.this,pckName));
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                }

            }
        });
        tv4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent;
                PackageManager pm = getPackageManager();

                String pckName = prefs.getString("4",
                        null);
                if(pckName!=null){
                    intent =
                            pm.getLaunchIntentForPackage(AdapterSearch.getPackNameByAppName(Drawer8Activity.this,pckName));
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                }

            }
        });
        tv5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent;
                PackageManager pm = getPackageManager();

                String pckName = prefs.getString("5",
                        null);
                if(pckName!=null){
                    intent =
                            pm.getLaunchIntentForPackage(AdapterSearch.getPackNameByAppName(Drawer8Activity.this,pckName));
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                }

            }
        });
        tv6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent;
                PackageManager pm = getPackageManager();

                String pckName = prefs.getString("6",
                        null);
                if(pckName!=null){
                    intent =
                            pm.getLaunchIntentForPackage(AdapterSearch.getPackNameByAppName(Drawer8Activity.this,pckName));
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                }

            }
        });
        tv7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent;
                PackageManager pm = getPackageManager();

                String pckName = prefs.getString("7",
                        null);
                if(pckName!=null){
                    intent =
                            pm.getLaunchIntentForPackage(AdapterSearch.getPackNameByAppName(Drawer8Activity.this,pckName));
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                }

            }
        });
        tv8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent;
                PackageManager pm = getPackageManager();

                String pckName = prefs.getString("8",
                        null);
                if(pckName!=null){
                    intent =
                            pm.getLaunchIntentForPackage(AdapterSearch.getPackNameByAppName(Drawer8Activity.this,pckName));
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                }

            }
        });

        ImageView luna = findViewById(R.id.imageView);
        luna.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(Drawer8Activity.this,
                        AppsActivity.class));
            }
        });

        if(!isMyLauncherDefault()) {
            AlertDialog alertDialog = new AlertDialog.Builder(this)
                    .setTitle("Set as defualt launcher")
                    .setMessage("Select moon launcher as default launcher")
                    .setPositiveButton("Ok",
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    PackageManager p = getPackageManager();
                                    ComponentName cN = new ComponentName(Drawer8Activity.this,
                                            FakeLauncherActivity.class);
                                    p.setComponentEnabledSetting(cN, PackageManager.COMPONENT_ENABLED_STATE_ENABLED, PackageManager.DONT_KILL_APP);

                                    Intent selector = new Intent(Intent.ACTION_MAIN);
                                    selector.addCategory(Intent.CATEGORY_HOME);
                                    startActivity(selector);

                                    p.setComponentEnabledSetting(cN, PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP);
                                }
                            })
                    .show();
            alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(getColor(R.color.colorPrimary));
        }

    }


    boolean isMyLauncherDefault() {
        PackageManager localPackageManager = getPackageManager();
        Intent intent = new Intent("android.intent.action.MAIN");
        intent.addCategory("android.intent.category.HOME");
        String str = localPackageManager.resolveActivity(intent,
                PackageManager.MATCH_DEFAULT_ONLY).activityInfo.packageName;
        return str.equals(getPackageName());
    }

    void getandset(TextView tv, String num){
        String name = prefs.getString(num, null);
        if(name!=null){
            tv.setText(name);
        }
    }

    public String getAppNameFromPkgName(String packagename) {
        try {
            PackageManager packageManager =
                    Drawer8Activity.this.getPackageManager();
            ApplicationInfo info = packageManager.getApplicationInfo(packagename, PackageManager.GET_META_DATA);
            String appName = (String) packageManager.getApplicationLabel(info);
            return appName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
            return "";
        }
    }
}
