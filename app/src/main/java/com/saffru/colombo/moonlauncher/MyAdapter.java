package com.saffru.colombo.moonlauncher;


import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.core.content.res.ResourcesCompat;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

public class MyAdapter extends BaseAdapter {
    private static List<String> list_nomi;
    List<Drawable> listIcons;
    static Context context;

    private static HashMap<Integer, Boolean> mSelection = new HashMap<>();

    MyAdapter(Context context,
              List<String> list_nomi, List<Drawable> listIcons, List<String> list_className){
        this.context = context;
        this.list_nomi = list_nomi;
        this.listIcons = listIcons;
    }

    @Override
    public int getCount() {
        return list_nomi.size();
    }

    @Override
    public Object getItem(int i) {
        return list_nomi.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        view = LayoutInflater.from(context).
                inflate(R.layout.item_app, viewGroup, false);
        view.setBackgroundColor(Color.parseColor("#000000")); //default color

        if (mSelection.get(i) != null) {
            view.setBackgroundColor(Color.WHITE);// this is a selected position
            // so make it red
        }
        TextView tv_nome = view.findViewById(R.id.nome);
        ImageView icon = view.findViewById(R.id.imageView2);
        Typeface typeface = ResourcesCompat.getFont(context, R.font.mina_regular);
        tv_nome.setTypeface(typeface);

        icon.setImageDrawable(listIcons.get(i));
        tv_nome.setText(list_nomi.get(i));

        return view;
    }

    void setNewSelection(int position, boolean checked) {
        mSelection.put(position, checked);
        notifyDataSetChanged();
    }

    void removeSelection(int position) {
        mSelection.remove(position);
        notifyDataSetChanged();
    }

    void clearSelection() {
        mSelection = new HashMap<>();
        notifyDataSetChanged();
    }

    boolean isPositionChecked(int position) {
        Boolean result = mSelection.get(position);
        return result == null ? false : result;
    }

    static void setPref(){
        SharedPreferences prefs =
                PreferenceManager.getDefaultSharedPreferences(context);

        SharedPreferences.Editor editor = prefs.edit();

        Iterator myVeryOwnIterator = mSelection.keySet().iterator();
        int i = 0;
        while(myVeryOwnIterator.hasNext()) {
            i++;
            int key= (Integer) myVeryOwnIterator.next();
            boolean value = mSelection.get(key);
            if(value){
                editor.putString(String.valueOf(i), list_nomi.get(key));
                editor.apply();
            }
        }
    }
}
